/**
  * Created by farooq.mohmud on 11/12/2016.
  */
class Rational(n: Int, d: Int){

  require(d != 0)

  private val g = gcd(n.abs, d.abs)
  val numerator: Int = n / g
  val denominator: Int = d / g

  def this(n : Int) = this(n, 1)

  override def toString(): String = s"$numerator/$denominator"

  def +(that: Rational) : Rational = {
    new Rational(
      (numerator * that.denominator) + (that.numerator * denominator),
      denominator * that.denominator
    )
  }

  def +(i : Int) : Rational = {
    new Rational(numerator + i * denominator, denominator)
  }

  def - (that: Rational): Rational =
    new Rational(
      numerator * that.denominator - that.numerator * denominator,
      denominator * that.denominator
    )

  def - (i: Int): Rational =
    new Rational(numerator - i * denominator, denominator)

  def *(that: Rational) : Rational = {
    new Rational(
      numerator * that.numerator,
      denominator * that.denominator
    )
  }

  def * (i: Int): Rational =
    new Rational(numerator * i, denominator)

  def /(that: Rational) : Rational = {
    *(that.reciprocal)
  }

  def / (i: Int): Rational =
    new Rational(numerator, denominator * i)

  def <(that: Rational) : Boolean = {
    numerator * that.denominator < that.numerator * denominator
  }

  def >(that: Rational) : Boolean = {
    if(<(that)) false else true
  }

  def max(that : Rational) : Rational = {
    if(<(that)) that else this
  }

  def min(that: Rational) : Rational = {
    if(>(that)) that else this
  }

  private def reciprocal : Rational = {
    new Rational(denominator, numerator)
  }

  private def gcd(x: Int, y: Int) : Int = {
    if(y == 0) x else gcd(y, x % y)
  }
}