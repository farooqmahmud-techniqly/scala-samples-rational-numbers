/**
  * Created by fmahmud on 11/12/2016.
  */

import scala.io.Source

object CountLinesApp {
  def main(args: Array[String]): Unit = {

    if (args.length < 2) {
      println("USAGE: CountLinesApp [length] [filenames]")
      return
    }

    args.drop(1).foreach(arg => processFile(arg, args(0).toInt))
  }

  def processFile(filename: String, width: Int) = {

    def processLine(line: String) = {
      if (line.length > width) {
        println(s"${filename}[${line.length}]:${line.trim}")
      }
    }

    val source = Source.fromFile(filename)
    source.getLines().foreach(line => processLine(line))

  }
}
