/**
  * Created by farooq.mohmud on 11/12/2016.
  */
object MyApp {
  def main(args: Array[String]) : Unit = {

    val oneHalf = new Rational(1, 2)
    val twoThirds = new Rational(2, 3)
    val sum = oneHalf + twoThirds
    val product = oneHalf * twoThirds
    val quotient = oneHalf / twoThirds

    println(s"${oneHalf} + ${twoThirds} = ${sum}")
    println(s"${oneHalf} * ${twoThirds} = ${product}")
    println(s"${oneHalf} / ${twoThirds} = ${quotient}")

    println(s"${oneHalf} is less than ${twoThirds}? ${oneHalf < twoThirds}")
    println(s"${oneHalf} is greater than ${twoThirds}? ${oneHalf > twoThirds}")
    println(s"Max is ${oneHalf.max(twoThirds)}")
    println(s"Min is ${oneHalf.min(twoThirds)}")

    val three = new Rational(3)
    val four = new Rational(-4)

    println(s"${three} + ${four} = ${three + four}")

    val threeFourths = new Rational(12, 16)
    println(threeFourths)

    val negThreeFourths = new Rational(12, -16)
    println(negThreeFourths)

    println(new Rational(1, 2) + 3)

    implicit def intToRational(x: Int) = new Rational(x)

    println(3 + new Rational(1, 2))
  }
}
