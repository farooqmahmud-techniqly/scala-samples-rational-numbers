/**
  * Created by farooq.mohmud on 11/15/2016.
  */
object CreditCardApp {

  def getCreditCards(): List[CreditCard] = {
      return List(
        new CreditCard(3),
        new CreditCard(5),
        new CreditCard(10)
    )
  }

  def main(args: Array[String]) : Unit = {

    val creditCards = getCreditCards()
    val getPremiumWithTotal = CreditCard.getPremium(creditCards.length)_
    val allPremiums = creditCards.map(getPremiumWithTotal).sum
    println(allPremiums)
  }
}
