/**
  * Created by farooq.mohmud on 11/15/2016.
  */
class CreditCard(val baseFee: Double) {

  require(baseFee > 0)
}

object CreditCard {

  def getPremium(totalCards: Int)(creditCard: CreditCard) : Double = {
    return creditCard.baseFee / (totalCards * 0.335)
  }
}
