import java.io.File

/**
  * Created by fmahmud on 11/14/2016.
  */
object FileMatcher {

  def main(args: Array[String]) : Unit = {

    if(args.length != 3){
      println("USAGE: FileMatcher [path] [filter]")
      return
    }

    val path = args(0)
    val filter = args(1)
    val filterType = args(2).toInt

    val extensionMatcher : (String => Boolean) = _.endsWith(filter)
    val containsMatcher : (String => Boolean) = _.contains(filter)
    val regexMatcher : (String => Boolean) = _.matches(filter)

    val matchingFunctions: Array[String =>Boolean] = Array(
      extensionMatcher,
      containsMatcher,
      regexMatcher
    )

    fileMatcher(path, matchingFunctions(filterType)).foreach(f => println(f))

    def fileMatcher(path: String, matchingFunction: (String => Boolean)) : Array[File] = {
      val files = for{
        file <- new File(path).listFiles()
        if(matchingFunction(file.getName))
      } yield file

      files
    }
  }
}
